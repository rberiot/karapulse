// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use anyhow::{bail, Result};
use env_logger::{Builder, Env};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "command")]
enum Command {
    #[structopt(name = "play")]
    Play,
    #[structopt(name = "pause")]
    Pause,
    #[structopt(name = "next")]
    Next,
    #[structopt(name = "search")]
    Search {
        #[structopt(name = "search")]
        fields: String,
    },
    #[structopt(name = "enqueue-db")]
    EnqueueDB {
        #[structopt(name = "song-id")]
        id: i32,
        #[structopt(name = "user")]
        user: String,
    },
    #[structopt(name = "status")]
    Status,
    #[structopt(name = "history")]
    History { user: Option<String> },
    #[structopt(name = "all")]
    All,
    #[structopt(name = "restart-song")]
    RestartSong,
}

impl Command {
    fn get_url(&self, host: &str, port: u16) -> String {
        let action = match self {
            Command::Play => "play".to_string(),
            Command::Pause => "pause".to_string(),
            Command::Next => "next".to_string(),
            Command::Search { fields } => {
                let mut req = String::from("search/");
                req.push_str(fields);
                req
            }
            Command::EnqueueDB { id, user } => {
                let mut req = String::from("enqueue_db/");
                req.push_str(&id.to_string());
                req.push('/');
                req.push_str(user);
                req
            }
            Command::Status => "status".to_string(),
            Command::History { user } => match user {
                Some(user) => {
                    let mut req = String::from("history/");
                    req.push_str(user);
                    req
                }
                None => "history".to_string(),
            },
            Command::All => "all".to_string(),
            Command::RestartSong => "restart-song".to_string(),
        };

        format!("http://{}:{}/api/{}", host, port, action)
    }
}

#[derive(StructOpt)]
#[structopt(name = "karapulse-cli")]
struct Opt {
    #[structopt(default_value = "localhost", short = "h", long = "host")]
    host: String,
    #[structopt(subcommand)]
    cmd: Command,
    #[structopt(default_value = "1848", short = "p", long = "port")]
    port: u16,
}

fn main() -> Result<()> {
    Builder::from_env(Env::default().default_filter_or("warn")).init();
    let opt = Opt::from_args();

    let url = opt.cmd.get_url(&opt.host, opt.port);
    let resp = reqwest::blocking::get(url.as_str())?;

    if resp.status().is_success() {
        println!("{}", resp.text().unwrap());
        Ok(())
    } else if resp.status().is_server_error() {
        bail!("Server error")
    } else {
        bail!("Request failed; Status: {:?}", resp.status())
    }
}
