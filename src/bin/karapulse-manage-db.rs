// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use anyhow::Result;
use chrono::Duration;
use env_logger::{Builder, Env};
use itertools::Itertools;
use karapulse::crawler::Crawler;
use karapulse::db::DB;
use std::collections::HashMap;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "command")]
enum Command {
    #[structopt(name = "list-songs")]
    ListSongs,
    #[structopt(name = "add-song")]
    AddSong {
        path: PathBuf,
        artist: String,
        title: String,
        length: Option<i32>,
    },
    #[structopt(name = "search-song")]
    SearchSong { fields: String },
    #[structopt(name = "add-file")]
    AddFile { path: PathBuf },
    #[structopt(name = "add-dir")]
    AddDir { path: PathBuf },
    #[structopt(name = "list-history")]
    ListHistory { user: Option<String> },
    #[structopt(name = "add-history")]
    AddHistory { song: i64, user: String },
    #[structopt(name = "set-history-played")]
    SetHistoryPlayed { history: i64 },
    #[structopt(name = "history-not-played")]
    HistoryNotPlayed,
    #[structopt(name = "stats")]
    Stats,
}

#[derive(StructOpt)]
#[structopt(name = "manage-db")]
struct Opt {
    #[structopt(name = "db")]
    path: Option<PathBuf>,
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(Debug)]
struct Stat {
    played_count: u32,
    played_time: u32,
}

impl Stat {
    fn new() -> Self {
        Self {
            played_count: 0,
            played_time: 0,
        }
    }
}

fn stats(db: &DB) -> Result<()> {
    let mut played = HashMap::new();

    println!("Played songs:");
    for h in db.list_history()? {
        if h.played.is_some() {
            let stat = played.entry(h.user.to_string()).or_insert_with(Stat::new);
            stat.played_count += 1;
            let song = karapulse::karapulse::Song::from_history_song(h.song);
            stat.played_time += song.duration();
        }
    }

    for (user, stat) in played
        .iter()
        .sorted_by(|(_, a), (_, b)| b.played_count.cmp(&a.played_count))
    {
        let d = Duration::seconds(i64::from(stat.played_time));
        println!(
            "  {}: {} ({}h {}m)",
            user,
            stat.played_count,
            d.num_hours(),
            d.num_minutes() % 60
        );
    }

    Ok(())
}

fn main() -> Result<()> {
    Builder::from_env(Env::default().default_filter_or("warn")).init();

    karapulse::common::init()?;

    let opt = Opt::from_args();

    let db = DB::new(&opt.path)?;
    let crawler = Crawler::new(&db);

    match opt.cmd {
        Command::ListSongs => {
            println!("Listing songs:");
            for song in db.list_songs()? {
                println!("{:?}", song);
            }
        }
        Command::AddSong {
            path,
            artist,
            title,
            length,
        } => {
            db.add_song(&path, &artist, &title, length)?;
            println!("Added song");
        }
        Command::SearchSong { fields } => {
            println!("Searching song matching '{}':", fields);
            for song in db.search(&fields)? {
                println!("{:?}", song);
            }
        }
        Command::AddFile { path } => {
            println!("Adding file {}", path.display());
            crawler.add_file(&path)?;
        }
        Command::AddDir { path } => {
            println!("Adding directory {}", path.display());
            crawler.add_dir(&path)?;
        }
        Command::ListHistory { user } => {
            let history = match user {
                Some(user) => {
                    println!("Listing history for {}:", user);
                    db.history_for(&user)?
                }
                None => {
                    println!("Listing history");
                    db.list_history()?
                }
            };

            for h in history {
                println!("{:?}", h);
            }
        }
        Command::AddHistory { song, user } => {
            let s = db.find_song(song)?;
            let id = db.add_history_db(&s, &user)?;
            println!("Added history: {}", id);
        }
        Command::SetHistoryPlayed { history } => {
            db.set_history_played(history)?;
            println!("History set as played");
        }
        Command::HistoryNotPlayed => {
            println!("Not played history:");
            for h in db.history_not_played()? {
                println!("{:?}", h);
            }
        }
        Command::Stats => {
            stats(&db)?;
        }
    }

    Ok(())
}
