// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use anyhow::{bail, Result};
use chrono::{DateTime, TimeZone, Utc};
use rusqlite::types::ToSql;
use rusqlite::{Connection, Row, Rows};
use std::path::{Path, PathBuf};

const DB_FILE_NAME: &str = "db-3.sqlite3";

#[derive(Debug)]
pub struct DB {
    conn: Connection,
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Song {
    pub rowid: i64,
    pub path: String,
    pub artist: String,
    pub title: String,
    pub length: Option<i32>,
}

impl Song {
    fn new_from_row(row: &Row) -> Result<Song, rusqlite::Error> {
        Ok(Song {
            rowid: row.get(0)?,
            path: row.get(1)?,
            artist: row.get(2)?,
            title: row.get(3)?,
            length: row.get(4)?,
        })
    }

    pub fn path(&self) -> PathBuf {
        Path::new(&self.path).to_path_buf()
    }
}

#[derive(Debug)]
pub struct History {
    pub rowid: i64,
    pub user: String,
    pub song: HistorySong,
    pub queued: DateTime<Utc>,
    pub played: Option<DateTime<Utc>>,
}

#[derive(Debug)]
pub enum HistorySong {
    Db(Song),
    Path(PathBuf),
}

impl HistorySong {
    #[cfg(test)]
    pub fn db_song(&self) -> &Song {
        match self {
            HistorySong::Db(song) => song,
            _ => unreachable!(),
        }
    }

    #[cfg(test)]
    pub fn path_song(&self) -> &PathBuf {
        match self {
            HistorySong::Path(p) => p,
            _ => unreachable!(),
        }
    }
}

#[derive(Debug)]
pub enum ExternalHistory {
    Path(PathBuf),
}

impl std::str::FromStr for ExternalHistory {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(p) = s.strip_prefix("path:") {
            Ok(ExternalHistory::Path(p.into()))
        } else {
            anyhow::bail!("invalid external history: {}", s);
        }
    }
}

impl std::fmt::Display for ExternalHistory {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            ExternalHistory::Path(p) => format!("path:{}", p.to_str().unwrap()),
        };

        write!(f, "{}", s)
    }
}

impl History {
    fn new_from_row(row: &Row) -> anyhow::Result<History> {
        let external: Option<String> = row.get(9)?;
        let song = if let Some(external) = external {
            match external.parse()? {
                ExternalHistory::Path(p) => HistorySong::Path(p),
            }
        } else {
            let song = Song {
                rowid: row.get(4)?,
                path: row.get(5)?,
                artist: row.get(6)?,
                title: row.get(7)?,
                length: row.get(8)?,
            };

            HistorySong::Db(song)
        };

        let queued = Utc.timestamp(row.get(2)?, 0);
        let played = match row.get(3) {
            Ok(t) => Some(Utc.timestamp(t, 0)),
            Err(_e) => None,
        };

        Ok(History {
            rowid: row.get(0)?,
            user: row.get(1)?,
            song,
            queued,
            played,
        })
    }
}

pub fn escape_search(identifier: &str) -> String {
    format!(
        "\"{}\"*",
        identifier.replace('\'', "''").replace('\"', "\"\"")
    )
}

impl DB {
    pub fn new(path: &Option<PathBuf>) -> Result<DB> {
        let db_path = match path {
            Some(p) => p.clone(),
            None => {
                let xdg_dirs = xdg::BaseDirectories::with_prefix("karapulse")?;
                xdg_dirs.place_data_file(DB_FILE_NAME)?
            }
        };

        let create = if !db_path.exists() {
            debug!("{:?} doesn't exist; creating it", db_path);
            true
        } else {
            false
        };

        info!("Opening {:?}", db_path);
        let conn = Connection::open(db_path)?;
        if create {
            DB::create_tables(&conn)?;
        }

        Ok(DB { conn })
    }

    #[allow(dead_code)]
    pub fn new_memory() -> Result<DB> {
        let conn = Connection::open_in_memory()?;
        DB::create_tables(&conn)?;
        Ok(DB { conn })
    }

    fn create_tables(conn: &Connection) -> Result<()> {
        conn.execute_batch(
            "
            CREATE TABLE songs (
              path VARCHAR NOT NULL,
              artist VARCHAR NOT NULL,
              title VARCHAR NOT NULL,
              length INTEGER
            );

            CREATE VIRTUAL TABLE fts_songs USING fts5(artist, title, content=songs, content_rowid=rowid);

            CREATE TRIGGER songs_ai AFTER INSERT ON songs BEGIN
              INSERT INTO fts_songs(rowid, artist, title) VALUES (new.rowid, new.artist, new.title);
            END;
            CREATE TRIGGER songs_ad AFTER DELETE ON songs BEGIN
              INSERT INTO fts_songs(fts_songs, rowid, artist, title) VALUES('delete', old.rowid, old.artist, old.title);
            END;
            CREATE TRIGGER songs_au AFTER UPDATE ON songs BEGIN
              INSERT INTO fts_songs(fts_songs, rowid, artist, title) VALUES('delete', old.rowid, old.artist, old.title);
              INSERT INTO fts_songs(rowid, artist, title) VALUES (new.rowid, new.artist, new.title);
            END;

            CREATE TABLE history (
              user TEXT NOT NULL,
              -- set when song is from the local DB
              song INT,
              -- set when song is from an external source, see ExternalHistory enum
              external VARCHAR,
              queued INT NOT NULL,
              played INT,
              FOREIGN KEY(song) REFERENCES songs(rowid)
            );
            ",
        )?;
        Ok(())
    }

    /* FIXME: return an iterator */
    pub fn list_songs(&self) -> Result<Vec<Song>> {
        let mut stmt = self
            .conn
            .prepare("SELECT rowid, path, artist, title, length FROM songs")?;

        let iter = stmt.query_map([], Song::new_from_row)?;
        let mut result = Vec::new();
        iter.for_each(|x| result.push(x.unwrap()));
        Ok(result)
    }

    #[allow(dead_code)]
    pub fn add_song(
        &self,
        path: &Path,
        artist: &str,
        title: &str,
        length: Option<i32>,
    ) -> Result<()> {
        self.conn.execute(
            "INSERT INTO songs (path, artist, title, length)
                  VALUES (?1, ?2, ?3, ?4)",
            [
                &path.to_str().unwrap(),
                &artist,
                &title,
                &length as &dyn ToSql,
            ],
        )?;
        Ok(())
    }

    pub fn search(&self, fields: &str) -> Result<Vec<Song>> {
        let mut stmt = self
            .conn
            .prepare("SELECT songs.rowid, path, songs.artist, songs.title, length FROM songs, fts_songs WHERE fts_songs match ?1 AND fts_songs.rowid = songs.rowid ORDER BY RANK;")?;

        let fields = fields
            .split(' ')
            .map(escape_search)
            .collect::<Vec<String>>()
            .join(" AND ");
        let iter = stmt.query_map([&fields], Song::new_from_row)?;
        let mut result = Vec::new();
        iter.for_each(|x| result.push(x.unwrap()));
        Ok(result)
    }

    pub fn find_song(&self, id: i64) -> Result<Song> {
        let mut stmt = self
            .conn
            .prepare("SELECT songs.rowid, path, songs.artist, songs.title, length FROM songs, fts_songs WHERE songs.rowid = ?1 AND fts_songs.rowid = songs.rowid;")?;

        let mut iter = stmt.query_map([id], Song::new_from_row)?;

        match iter.next() {
            Some(song) => Ok(song.unwrap()),
            None => bail!("Song not found"),
        }
    }

    fn return_vec_history(&self, mut rows: Rows) -> Result<Vec<History>> {
        let mut result = Vec::new();
        while let Some(row) = rows.next()? {
            let h = History::new_from_row(row)?;
            result.push(h);
        }
        Ok(result)
    }

    pub fn list_history(&self) -> Result<Vec<History>> {
        let mut stmt = self.conn.prepare(
            "SELECT history.rowid, user, queued, played, songs.rowid, path, artist, title, length, external FROM history LEFT OUTER JOIN songs on history.song = songs.rowid WHERE played IS NOT NULL ORDER BY queued DESC",
        )?;

        let rows = stmt.query([])?;
        self.return_vec_history(rows)
    }

    pub fn history_for(&self, user: &str) -> Result<Vec<History>> {
        let mut stmt = self.conn.prepare(
            "SELECT history.rowid, user, queued, played, songs.rowid, path, artist, title, length, external FROM history LEFT OUTER JOIN songs on history.song = songs.rowid WHERE user = ?1 AND played IS NOT NULL ORDER BY queued DESC",
        )?;

        let rows = stmt.query([&user])?;
        self.return_vec_history(rows)
    }

    pub fn add_history_db(&self, song: &Song, user: &str) -> Result<i64> {
        self.conn.execute(
            "INSERT INTO history (user, song, queued)
                  VALUES (?1, ?2, strftime('%s','now'))",
            [&user, &song.rowid as &dyn ToSql],
        )?;
        Ok(self.conn.last_insert_rowid())
    }

    pub fn add_history_external(&self, external: ExternalHistory, user: &str) -> Result<i64> {
        self.conn.execute(
            "INSERT INTO history (user, external, queued)
                  VALUES (?1, ?2, strftime('%s','now'))",
            [user, external.to_string().as_str()],
        )?;
        Ok(self.conn.last_insert_rowid())
    }

    pub fn set_history_played(&self, history: i64) -> Result<()> {
        self.conn.execute(
            "UPDATE history SET played = strftime('%s','now') WHERE rowid = ?",
            [&history as &dyn ToSql],
        )?;
        Ok(())
    }

    pub fn delete_history(&self, history: i64) -> Result<()> {
        self.conn.execute(
            "DELETE FROM history WHERE rowid = ?",
            [&history as &dyn ToSql],
        )?;
        Ok(())
    }

    pub fn history_not_played(&self) -> Result<Vec<History>> {
        let mut stmt = self.conn.prepare(
            "SELECT history.rowid, user, queued, played, songs.rowid, path, artist, title, length, external FROM history LEFT OUTER JOIN songs on history.song = songs.rowid WHERE played IS NULL ORDER BY queued ASC",
        )?;

        let rows = stmt.query([])?;
        self.return_vec_history(rows)
    }

    #[allow(dead_code)]
    pub fn has_song(&self, path: &Path) -> Result<bool> {
        let mut stmt = self
            .conn
            .prepare("SELECT rowid, path, artist, title, length FROM songs WHERE path = ?1")?;

        let mut rows = stmt.query([&path.to_str().unwrap()])?;
        Ok(rows.next()?.is_some())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn list_and_add() {
        let db = DB::new_memory().unwrap();
        assert_eq!(db.list_songs().unwrap().len(), 0);

        let path = PathBuf::from("file1.mp3");
        assert!(!db.has_song(&path).unwrap());
        db.add_song(&path, "artist1", "title1", None).unwrap();
        assert!(db.has_song(&path).unwrap());

        let songs = db.list_songs().unwrap();
        assert_eq!(songs.len(), 1);
        assert_eq!(songs[0].path, "file1.mp3");
        assert_eq!(songs[0].artist, "artist1");
        assert_eq!(songs[0].title, "title1");
        assert_eq!(songs[0].length, None);

        db.add_song(&PathBuf::from("file2.mp3"), "artist1", "title2", Some(10))
            .unwrap();
        let songs = db.list_songs().unwrap();
        assert_eq!(songs.len(), 2);
        assert_eq!(songs[1].path, "file2.mp3");
        assert_eq!(songs[1].artist, "artist1");
        assert_eq!(songs[1].title, "title2");
        assert_eq!(songs[1].length, Some(10));

        db.add_song(
            &PathBuf::from("file3.mp3"),
            "artist'2",
            "title\"3",
            Some(10),
        )
        .unwrap();
        let songs = db.list_songs().unwrap();
        assert_eq!(songs.len(), 3);
        assert_eq!(songs[2].path, "file3.mp3");
        assert_eq!(songs[2].artist, "artist'2");
        assert_eq!(songs[2].title, "title\"3");
    }

    #[test]
    fn search() {
        let db = DB::new_memory().unwrap();

        assert_eq!(db.search("badger").unwrap().len(), 0);

        db.add_song(&PathBuf::from("file1.mp3"), "artist1 rock", "title1", None)
            .unwrap();
        db.add_song(&PathBuf::from("file2.mp3"), "artist1 sux", "title2", None)
            .unwrap();
        db.add_song(&PathBuf::from("file3.mp3"), "badger", "title3", None)
            .unwrap();
        db.add_song(&PathBuf::from("file4.mp3"), "artist2", "title4", None)
            .unwrap();

        let songs = db.search("artist1").unwrap();
        assert_eq!(songs.len(), 2);
        assert_eq!(songs[0].path, "file1.mp3");
        assert_eq!(songs[1].path, "file2.mp3");

        let songs = db.search("title2").unwrap();
        assert_eq!(songs.len(), 1);
        assert_eq!(songs[0].path, "file2.mp3");

        let songs = db.search("mushroom").unwrap();
        assert_eq!(songs.len(), 0);

        let songs = db.search("artist1 title1").unwrap();
        assert_eq!(songs.len(), 1);
        assert_eq!(songs[0].path, "file1.mp3");

        let song = db.find_song(1).unwrap();
        assert_eq!(song.path, "file1.mp3");

        assert!(db.find_song(666).is_err());

        assert_eq!(db.search("J'aime").unwrap().len(), 0);
        assert_eq!(db.search("J\"aime").unwrap().len(), 0);

        // prefix search
        let songs = db.search("artist").unwrap();
        assert_eq!(songs.len(), 3);
    }

    #[test]
    fn history() {
        let db = DB::new_memory().unwrap();

        db.add_song(&PathBuf::from("file1.mp3"), "artist1", "title1", None)
            .unwrap();
        db.add_song(&PathBuf::from("file2.mp3"), "artist1", "title2", None)
            .unwrap();

        let history = db.list_history().unwrap();
        assert_eq!(history.len(), 0);
        let songs = db.list_songs().unwrap();
        assert_eq!(songs.len(), 2);

        let song = &songs[0];
        let id = db.add_history_db(song, "Alice").unwrap();
        db.set_history_played(id).unwrap();
        let id2 = db
            .add_history_external(ExternalHistory::Path("/file3.mp3".into()), "Bob")
            .unwrap();
        db.set_history_played(id2).unwrap();

        let history = db.list_history().unwrap();
        assert_eq!(history.len(), 2);

        let h = &history[0];
        assert_eq!(h.user, "Alice");
        assert_eq!(h.song.db_song(), song);
        assert_eq!(h.queued.date(), Utc::today());
        assert!(h.played.is_some());

        let h = &history[1];
        assert_eq!(h.user, "Bob");
        assert_eq!(h.song.path_song(), Path::new("/file3.mp3"));
        assert_eq!(h.queued.date(), Utc::today());
        assert!(h.played.is_some());
    }

    #[test]
    fn history_for() {
        let db = DB::new_memory().unwrap();

        db.add_song(&PathBuf::from("file1.mp3"), "artist1", "title1", None)
            .unwrap();
        db.add_song(&PathBuf::from("file2.mp3"), "artist1", "title2", None)
            .unwrap();

        let songs = db.list_songs().unwrap();

        let id = db.add_history_db(&songs[0], "Alice").unwrap();
        db.set_history_played(id).unwrap();
        let id = db.add_history_db(&songs[1], "Bob").unwrap();
        db.set_history_played(id).unwrap();
        let id = db.add_history_db(&songs[1], "Alice").unwrap();
        db.set_history_played(id).unwrap();
        let id = db
            .add_history_external(ExternalHistory::Path("/file3.mp3".into()), "Bob")
            .unwrap();
        db.set_history_played(id).unwrap();

        let history = db.history_for("Alice").unwrap();
        assert_eq!(history.len(), 2);
        assert_eq!(history[0].user, "Alice");
        assert_eq!(history[1].user, "Alice");

        let history = db.history_for("Bob").unwrap();
        assert_eq!(history.len(), 2);
        assert_eq!(history[0].user, "Bob");
        assert_eq!(history[0].song.db_song(), &songs[1]);
        assert_eq!(history[1].user, "Bob");
        assert_eq!(history[1].song.path_song(), Path::new("/file3.mp3"));

        let history = db.history_for("Oscar").unwrap();
        assert_eq!(history.len(), 0);
    }

    #[test]
    fn history_not_played() {
        let db = DB::new_memory().unwrap();

        assert_eq!(db.history_not_played().unwrap().len(), 0);

        db.add_song(&PathBuf::from("file1.mp3"), "artist1", "title1", None)
            .unwrap();

        let songs = db.list_songs().unwrap();

        let fst = db.add_history_db(&songs[0], "Alice").unwrap();
        let snd = db.add_history_db(&songs[0], "Bob").unwrap();
        let thd = db
            .add_history_external(ExternalHistory::Path("/file3.mp3".into()), "Bob")
            .unwrap();

        let history = db.history_not_played().unwrap();
        assert_eq!(history.len(), 3);
        assert_eq!(history[0].user, "Alice");
        assert_eq!(history[1].user, "Bob");
        assert_eq!(history[2].user, "Bob");

        db.set_history_played(fst).unwrap();
        assert_eq!(db.history_not_played().unwrap().len(), 2);

        db.set_history_played(snd).unwrap();
        assert_eq!(db.history_not_played().unwrap().len(), 1);

        db.set_history_played(thd).unwrap();
        assert_eq!(db.history_not_played().unwrap().len(), 0);
    }

    #[test]
    fn delete_history() {
        let db = DB::new_memory().unwrap();

        assert_eq!(db.history_not_played().unwrap().len(), 0);

        db.add_song(&PathBuf::from("file1.mp3"), "artist1", "title1", None)
            .unwrap();

        let songs = db.list_songs().unwrap();
        let id = db.add_history_db(&songs[0], "Alice").unwrap();
        db.set_history_played(id).unwrap();

        let history = db.history_for("Alice").unwrap();
        assert_eq!(history.len(), 1);

        db.delete_history(id).unwrap();
        let history = db.history_for("Alice").unwrap();
        assert_eq!(history.len(), 0);
    }
}
