// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

// Structures returned as JSON reply. Be careful to not change their serialization
// as that would be a protocol break.

use std::{convert::TryFrom, path::Path, str::FromStr};

use chrono::{DateTime, Utc};
use serde::Serialize;

use crate::{db, karapulse, queue};

#[derive(Debug, PartialEq, Eq)]
pub enum SongId {
    Db(i64),
}

impl Serialize for SongId {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self {
            SongId::Db(id) => serializer.serialize_str(&format!("db-{}", id)),
        }
    }
}

impl FromStr for SongId {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(id) = s.strip_prefix("db-") {
            let id = i64::from_str(id)?;
            Ok(SongId::Db(id))
        } else {
            Err(anyhow::anyhow!("unsupported prefix"))
        }
    }
}

impl std::fmt::Display for SongId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SongId::Db(id) => write!(f, "db-{}", id),
        }
    }
}

// search
#[derive(Debug, Serialize)]
pub struct Song {
    pub id: SongId,
    pub artist: String,
    pub title: String,
    pub length: Option<i32>,
    #[serde(rename = "type")]
    pub type_: SongType,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum SongType {
    Cdg,
    Video,
}

impl Song {
    fn new(s: karapulse::Song) -> Option<Self> {
        match s {
            karapulse::Song::Db(s) => Some(Self::from_db(s)),
            karapulse::Song::Path(_) => None,
        }
    }

    pub fn from_db(s: db::Song) -> Self {
        // assume mp3 files are cdg and the rest is a video file
        let type_ = match Path::new(&s.path).extension().map(|ext| ext.to_str()) {
            Some(Some("mp3")) => SongType::Cdg,
            _ => SongType::Video,
        };

        Self {
            id: SongId::Db(s.rowid),
            artist: s.artist,
            title: s.title,
            length: s.length,
            type_,
        }
    }
}

#[derive(Debug, Serialize)]
pub struct SearchResponse(Vec<Song>);

impl SearchResponse {
    pub fn new(songs: Vec<Song>) -> Self {
        Self(songs)
    }
}

// history
#[derive(Debug, Serialize)]
struct History {
    pub id: i64,
    pub user: String,
    pub song: Option<Song>,
    pub queued: DateTime<Utc>,
    pub played: Option<DateTime<Utc>>,
}

impl std::convert::TryFrom<db::History> for History {
    type Error = anyhow::Error;

    fn try_from(s: db::History) -> Result<Self, Self::Error> {
        match s.song {
            db::HistorySong::Path(_) => Err(anyhow::anyhow!("path songs not sent to clients")),
            db::HistorySong::Db(song) => Ok(Self {
                id: s.rowid,
                user: s.user,
                song: Some(Song::from_db(song)),
                queued: s.queued,
                played: s.played,
            }),
        }
    }
}

#[derive(Debug, Serialize)]
pub struct HistoryResponse(Vec<History>);

impl HistoryResponse {
    pub fn new(history: Vec<db::History>) -> Self {
        Self(
            history
                .into_iter()
                .filter_map(|h| History::try_from(h).ok())
                .collect(),
        )
    }
}

// status
#[derive(Debug, Serialize)]
pub struct StatusResponseSong {
    pub user: String,
    pub song: Song,
    #[serde(skip_serializing_if = "Option::is_none")]
    eta: Option<u32>,
}

impl StatusResponseSong {
    pub fn new(item: queue::Item, eta: Option<u32>) -> Option<Self> {
        match Song::new(item.song) {
            Some(song) => Some(Self {
                user: item.user,
                song,
                eta,
            }),
            None => None,
        }
    }
}

#[derive(Debug, Serialize)]
pub struct StatusResponse {
    pub state: StateResponse,
    pub current_song: Option<StatusResponseSong>,
    pub position: Option<u64>,
    pub queue: Vec<StatusResponseSong>,
}

#[derive(Debug, Serialize)]
pub enum StateResponse {
    Waiting,
    Playing,
    Paused,
}

impl StatusResponse {
    pub fn new(
        state: karapulse::State,
        current_song: Option<StatusResponseSong>,
        position: Option<u64>,
        queue: Vec<StatusResponseSong>,
    ) -> Self {
        let state = match state {
            karapulse::State::Waiting => StateResponse::Waiting,
            karapulse::State::Playing | karapulse::State::Announcing => StateResponse::Playing,
            karapulse::State::Paused(_) => StateResponse::Paused,
        };

        Self {
            state,
            current_song,
            position,
            queue,
        }
    }
}
