import {Component, OnDestroy, OnInit} from '@angular/core';
import {PlayHistory, QueueEntry} from '../status';
import {MatBottomSheet} from '@angular/material';
import {KaraokeService} from '../karaoke.service';
import {UserService} from '../user.service';
import {SongDetailsComponent} from '../song-details/song-details.component';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  displayedColumns: string[] = ['user', 'artist', 'title'];
  historyItems: PlayHistory; // QueueEntry[];
  isLoading = true;

  constructor(private bottomSheet: MatBottomSheet, private karaokeService: KaraokeService, private userService: UserService) {
  }

  ngOnInit() {
    // this.karaokeService.playHistoryEvent$.subscribe(history => this.historyItems = history);
    this.karaokeService.playHistory().subscribe(history => {
      this.historyItems = history;
      this.isLoading = false;
    });
  }

  refreshQueue(): void {
    this.isLoading = true;
    this.karaokeService.playHistory().subscribe(history => {
      this.historyItems = history;
      this.isLoading = false;
    });
  }

  entryOfCurrentUser(row): boolean {
    return row.user === this.userService.profile.name.trim();
  }

  openBottomSheet(row): void {
    console.log(row);
    const song = row.song;
    this.bottomSheet.open(SongDetailsComponent, {data: song});
  }
}
