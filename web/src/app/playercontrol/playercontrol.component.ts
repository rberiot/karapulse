import {Component, OnInit} from '@angular/core';
import {KaraokeService} from '../karaoke.service';
import {State} from '../status';


@Component({
  selector: 'app-playercontrol',
  templateUrl: './playercontrol.component.html',
  styleUrls: ['./playercontrol.component.css']
})
export class PlayerControlComponent implements OnInit {
  playing = false;
  showControls = false;

  constructor(private karaokeService: KaraokeService) {
    karaokeService.playingEvent$.subscribe(playing => this.playing = playing);
  }

  ngOnInit() {

  }

  onNext() {
    this.karaokeService.next().subscribe();
  }
  onPlay() {
    this.karaokeService.play().subscribe();
  }
  onPause() {
    this.karaokeService.pause().subscribe();
  }

   onRestartSong() {
    this.karaokeService.restartSong().subscribe();
  }

  toggleVisible(visible: boolean) {
    this.showControls = visible;
  }
}
