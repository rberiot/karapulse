import {Component, Inject, OnInit} from '@angular/core';
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef} from '@angular/material';
import {KaraokeService} from '../karaoke.service';
import {QueueEntry} from '../status';
import {UserService} from '../user.service';

@Component({
  selector: 'app-song-details',
  templateUrl: './queue-entry.component.html',
  styleUrls: ['./queue-entry.component.css'],
})
export class QueueEntryComponent implements OnInit {

  constructor(private bottomSheetRef: MatBottomSheetRef<QueueEntryComponent>,
              @Inject(MAT_BOTTOM_SHEET_DATA) private queueEntry: QueueEntry,
              private karaokeService: KaraokeService,
              private userService: UserService) {
  }

  ngOnInit() {
  }

  openLink(event: MouseEvent): void {
    this.bottomSheetRef.dismiss();
    event.preventDefault();
  }

  canRemove() {
    return this.queueEntry.user === this.userService.profile.name.trim();
  }

  remove(event: MouseEvent) {
    this.bottomSheetRef.dismiss();
    this.karaokeService.removeQueuedEntry(this.queueEntry.song.id).subscribe();

    event.preventDefault();
  }
}
