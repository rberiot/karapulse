import {Component, OnDestroy, OnInit} from '@angular/core';
import {QueueEntry} from '../status';
import {MatBottomSheet} from '@angular/material';
import {KaraokeService} from '../karaoke.service';
import {Subject, Subscription} from 'rxjs';
import {QueueEntryComponent} from '../queue-entry/queue-entry.component';
import {UserService} from '../user.service';

@Component({
  selector: 'app-queue',
  templateUrl: './queue.component.html',
  styleUrls: ['./queue.component.css']
})
export class QueueComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['user', 'artist', 'title'];
  currentQueue: QueueEntry[];
  poller: Subscription;
  refreshTrigger: Subject<void>;

  constructor(private bottomSheet: MatBottomSheet, private karaokeService: KaraokeService, private userService: UserService) {
  }

  ngOnInit() {
    this.karaokeService.statusEvent$.subscribe(status => this.currentQueue = status.queue);
  }

  ngOnDestroy(): void {
    if (this.poller) {
      this.poller.unsubscribe();
    }
  }

  refreshQueue(): void {
    if (this.refreshTrigger) {
      this.refreshTrigger.next();
    }
  }

  entryOfCurrentUser(row): boolean {
    return row.user === this.userService.profile.name.trim();
  }

  openBottomSheet(row): void {
    console.log(row);
    if(this.entryOfCurrentUser(row)) {
      this.bottomSheet.open(QueueEntryComponent, {data: row});
    }
  }
}
