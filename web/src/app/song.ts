export class Song {
  id: string;
  artist: string;
  title: string;
  length: number;
}
