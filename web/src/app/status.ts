import {Song} from './song';

export class Status {
  state: State;
  currentSong: {
    user: string;
    song: Song;
    position
  };

queue: QueueEntry[];
}

export enum State {
  PLAYING = 'Playing',
  WAITING = 'Waiting',
  PAUSED = 'Paused',
}
export class QueueEntry {
  user: string;
  song: Song;
}

export type PlayHistory = QueueEntry[];
