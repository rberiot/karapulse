import {Injectable} from '@angular/core';
import {Profile} from './profile';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  profile: Profile;

  constructor() {
    // if the profile, or the use of the localstorage grows, we should use a dedicated typescript or ng lib to handle serialization;
    this.profile = new Profile();

    const savedName = localStorage.getItem('profile.name');
    this.profile.name = savedName ? savedName : 'default';
  }

  save(): void {
    localStorage.setItem('profile.name', this.profile.name ? this.profile.name : 'Céline Dion');
  }
}
